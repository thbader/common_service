## Projekt im lokalen Maven-Repo ablegen
Um das Projekt auch in anderen Projekten zu verwenden, sollte ein Artefakt gebaut werden, 
das im lokalen Repository abgelegt werden muss. 
``` 
mvn clean install
```
Dabei werden drei Artefakte erzeugt:
* Bibliothek
* Javadoc
* Quellcode

### Deployment 

```
mvn clean deploy
```
see https://central.sonatype.org/publish/release/#locate-and-examine-your-staging-repository
