package sh.bader.common.service.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import sh.bader.common.id.ID;
import sh.bader.common.id.IDEntity;
import sh.bader.common.service.Memento;
import sh.bader.common.validation.Errors;
import sh.bader.common.validation.Validation;
import sh.bader.common.validation.value.Value;

import static sh.bader.common.validation.ValidationException.onErrors;
import static sh.bader.common.validation.ValidationException.onErrorsWithOptionalReturn;

public abstract class AbstractEntityService<I extends ID<I, E>, E extends IDEntity<I, E>, R extends EntityServiceRequest> implements EntityService<I, E, R> {
    private final Logger log = LogManager.getLogger(this.getClass());

    protected abstract JpaRepository<E, I> getRepository();

    protected abstract R newRequest();

    /**
     * Creates a new instance of the entity.
     *
     * @return the entity instance.
     */
    protected abstract E newEntity();

    private Optional<E> validated(E entity, R request, Errors errors, Supplier<E> supplier) {
        Assert.notNull(supplier, "supplier is null");

        final Validation validation = doValidate(entity, request, errors);
        if (errors.hasErrors()) {
            this.log.debug("validate failed: request={}, errors={}", request, errors);
            return Optional.empty();
        }

        if (!validation.isChanged()) {
            this.log.debug("update rejected: no changes on entity(id={}): {}", entity.getId(), request);
            return Optional.of(entity);
        }

        if (this.log.isDebugEnabled() && entity.getId() != null) {
            List<String> changes = getChanges(validation);
            if (!changes.isEmpty()) {
                this.log.debug("changes(id={}): {}", ID.getId(entity), changes);
            }
        }
        return Optional.of(supplier.get());
    }

    private Validation doValidate(final E entity, final R request, Errors errors) {
        Assert.notNull(entity, "entity is null");
        Assert.notNull(request, "request is null");
        Assert.notNull(errors, "errors is null");

        preValidate(entity, errors);
        if (errors.hasErrors()) {
            this.log.debug("prevalidate failed: {}", errors);
        }

        final Validation validation = new Validation(entity.getId() == null, errors);
        this.validate(entity, request, validation);
        return validation;
    }

    protected void preValidate(E entity, Errors errors) {
    }

    public final void validate(final E entity, final R request, final Errors errors) {
        final Validation validation = new Validation(entity.getId() == null, errors);
        this.validate(entity, request, validation);
    }

    public abstract void validate(final E entity, final R request, final Validation validation);

    protected abstract void merge(final boolean isNew, final E entity, final R request);

    @Override
    @Transactional
    public E create(final Consumer<R> consumer) {
        R request = newRequest();
        consumer.accept(request);

        return this.create(request);
    }

    @Override
    @Transactional
    public E create(final R request) {
        return onErrorsWithOptionalReturn(errors -> this.create(request, errors));
    }

    @Override
    @Transactional
    public Optional<E> create(final Consumer<R> consumer, final Errors errors) {
        R request = newRequest();
        consumer.accept(request);

        return this.create(request, errors);
    }

    @Override
    @Transactional
    public Optional<E> create(final R request, final Errors errors) {
        final E entity = this.newEntity();

        return this.validated(entity, request, errors, () -> {
            this.merge(true, entity, request);
            return doCreate(entity, request);
        });
    }

    protected E doCreate(E entity, R request) {
        E createdEntity = this.getRepository().saveAndFlush(entity);

        this.log.debug("created entity(id={}): {}", createdEntity.getId(), request);

        return createdEntity;
    }

    @Override
    @Transactional
    public E update(final E entity, final Consumer<R> consumer) {
        R request = newRequest();
        consumer.accept(request);

        return this.update(entity, request);
    }

    @Override
    @Transactional
    public E update(final E entity, final R request) {
        return onErrorsWithOptionalReturn(errors -> this.update(entity, request, errors));
    }

    @Override
    @Transactional
    public Optional<E> update(final E entity, final Consumer<R> consumer, final Errors errors) {
        R request = newRequest();
        consumer.accept(request);

        return this.update(entity, request, errors);
    }

    @Override
    @Transactional
    public Optional<E> update(final E entity, final R request, final Errors errors) {

        Memento<E> memento = createMemento(entity);
        return this.validated(entity, request, errors, () -> {
            this.merge(false, entity, request);
            return doUpdate(entity, request, memento);
        });
    }

    protected E doUpdate(E entity, R request, Memento<E> memento) {
        E updatedEntity = getRepository().saveAndFlush(entity);

        this.log.debug("updated entity(id={}): {}", entity.getId(), request);

        return updatedEntity;
    }

    protected Memento<E> createMemento(final E entity) {
        return null;
    }

    @Override
    @Transactional
    public void delete(final E entity) {
        onErrors(errors -> this.delete(entity, errors));
    }

    @Override
    @Transactional
    public void delete(final E entity, final Errors errors) {
        if (entity == null) {
            return;
        }

        this.validateOnDelete(entity, errors);

        if (errors.hasErrors()) {
            return;
        }

        doDelete(entity);
    }

    protected void doDelete(E entity) {
        getRepository().delete(entity);
        getRepository().flush();

        this.log.debug("deleted entity(id={})", entity.getId());
    }

    protected void validateOnDelete(final E entity, final Errors errors) {

    }

    @Override
    @Transactional
    public void delete(final Collection<E> entities) {
        onErrors(errors -> this.delete(entities, errors));
    }

    @Override
    @Transactional
    public void delete(final Collection<E> entities, final Errors errors) {
        if (entities == null) {
            return;
        }

        new ArrayList<>(entities).forEach(entity -> this.delete(entity, errors));
    }

    private static List<String> getChanges(Validation validation) {
        return validation.getValues().stream()
                .filter(Value::isChanged)
                .map(value -> {
                    String oldValue = value.onlyEntity().getValue()
                            .map(AbstractEntityService::toValueString)
                            .orElse(null);
                    String newValue = value.onlyInput().getValue()
                            .map(AbstractEntityService::toValueString)
                            .orElse(null);

                    return value.getPath() + " = " + oldValue + " -> " + newValue;
                })
                .collect(Collectors.toList());
    }

    private static String toValueString(Object value) {
        if (value instanceof IDEntity) {
            return String.valueOf(ID.getId((IDEntity) value));
        }

        if (value instanceof Collection) {
            String change = ((Collection<Object>) value).stream()
                    .map(AbstractEntityService::toValueString)
                    .collect(Collectors.joining(","));
            if (StringUtils.isBlank(change)) {
                return String.valueOf((Object) null);
            }
            return change;
        }

        return String.valueOf(value);
    }
}
