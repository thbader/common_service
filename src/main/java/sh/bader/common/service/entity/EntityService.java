package sh.bader.common.service.entity;

import java.util.Collection;
import java.util.Optional;
import java.util.function.Consumer;

import sh.bader.common.id.ID;
import sh.bader.common.id.IDEntity;
import sh.bader.common.validation.Errors;

public interface EntityService<I extends ID<I, E>, E extends IDEntity<I, E>, R extends EntityServiceRequest> {
    E create(R request);

    E create(final Consumer<R> consumer);

    Optional<E> create(R request, Errors errors);

    Optional<E> create(final Consumer<R> consumer, final Errors errors);

    E update(E entity, R request);

    E update(final E entity, final Consumer<R> consumer);

    Optional<E> update(E entity, R request, Errors errors);

    Optional<E> update(final E entity, final Consumer<R> consumer, final Errors errors);

    void delete(E entity);

    void delete(E entity, Errors errors);

    void delete(Collection<E> entities);

    void delete(Collection<E> entities, Errors errors);
}
