package sh.bader.common.service;

import sh.bader.common.id.IDEntity;

/**
 * Interface for mementos.
 *
 * @param <E> the type of the memento.
 */
public interface Memento<E extends IDEntity<?, E>> {

    /**
     * Check if the entity has changed.
     *
     * @param entity the entity.
     * @return {@code true}, if the entity has changed, {@code false} otherwise.
     */
    boolean hasChanged(E entity);
}
